//
//  ShowInformationViewController.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 5/18/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import UIKit

class ShowInformationViewController: UIViewController {

    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var photoWidth: NSLayoutConstraint!
    
    private var contact : User!
    private var contactList = (UIApplication.shared.delegate as! AppDelegate).contactList
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageAnimation()
        

        self.setGeadient()
        contact = contactList.getContactByUuid(uuid: contactList.showedContactUuid)
        self.setView()
        contactList.setContactIsEdited(state: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func editContact(_ sender: Any) {
        contactList.setContactIsEdited(state: true)
    }
    
    func setView() {
        
        photoWidth.constant = self.view.frame.width * 0.75
        lblFullName.text = self.contact.name + " " + self.contact.surname
        lblPhoneNumber.text = self.contact.phoneNumber
        lblEmail.text = self.contact.eMail
        userPhoto.image = UIImage(data: contact.avatar,scale:1.0)
        self.title = self.contact.name + " " + self.contact.surname
    }
    
    @IBAction func returnToRoot(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setGeadient() {
        let firstColor = UIColor(red: 121/255, green: 204/255, blue: 251/255, alpha: 1.0).cgColor
        let secondColor = UIColor(red: 92/255, green: 239/255, blue: 252/255, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [firstColor, secondColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func imageAnimation() {
        let degree : Double = 90
        let rotationAngle = CGFloat(degree * .pi / 180)
        let rotationTransform = CATransform3DMakeRotation(rotationAngle, 1, 0, 0)
        self.userPhoto.layer.transform = rotationTransform
        
        UIView.animate(withDuration: 1, delay: 0.25, options: .curveEaseOut, animations: {
            self.userPhoto.layer.transform = CATransform3DIdentity
        })
   }
    
    func animateLabels() {

    }
    
    func labelAnimation(_ label: UILabel) {
        
        UIView.animate(withDuration: 1, animations: {
            label.alpha = 1
        })
    }
}
