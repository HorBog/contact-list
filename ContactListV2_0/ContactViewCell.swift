//
//  ContactViewCell.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 6/14/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import UIKit

class ContactViewCell: UITableViewCell {

    var fullNameTxt = ""
    var firstNameTxt = ""
    var buttonState  = false
    
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var eMail: UILabel!
    @IBOutlet weak var btnShowHideContactInfo: UIButton!
    
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var phoneToFirstName: NSLayoutConstraint!
    @IBOutlet weak var phoneToBottom: NSLayoutConstraint!
    
    @IBOutlet weak var lastNameToFirstName: NSLayoutConstraint!
    @IBOutlet weak var phoneNumberToLastName: NSLayoutConstraint!
    @IBOutlet weak var eMailToPhone: NSLayoutConstraint!
    @IBOutlet weak var eMailToBottom: NSLayoutConstraint!
    
    
    @IBAction func btnShowHideContactInfo(_ sender: Any) {
        
        changeCellExpand()
        let tableView = self.superview?.superview as! UITableView
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func changeCellExpand() {
        
        if buttonState == true {
        
            btnShowHideContactInfo.setTitle("▲", for: UIControlState.normal)
            
            
            imageWidth.constant = 0.25 * self.frame.width
            lastName.isHidden = false
            eMail.isHidden = false
        
            firstName.text = firstNameTxt
            
            phoneToFirstName.priority = 10
            phoneToBottom.priority = 10
            
            lastNameToFirstName.priority = 750
            phoneNumberToLastName.priority = 750
            eMailToPhone.priority = 750
            eMailToBottom.priority = 750
            
            buttonState = false
            
        } else {
            
            lastName.isHidden = true
            eMail.isHidden = true
            imageWidth.constant = 0.2 * self.frame.width
        
            firstName.text = fullNameTxt
            
            phoneToFirstName.priority = 999
            phoneToBottom.priority = 999
            phoneToBottom.constant = 5
            phoneToFirstName.constant = 5
            
            lastNameToFirstName.priority = 10
            phoneNumberToLastName.priority = 10
            eMailToPhone.priority = 10
            eMailToBottom.priority = 10
            
            btnShowHideContactInfo.setTitle("▼", for: UIControlState.normal)
            
            buttonState = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
