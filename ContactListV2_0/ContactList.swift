//
//  ContactList.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 5/17/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import Foundation

protocol contactListProtocol {
    var showedContactUuid : String {get set}
    var contactIsEdited : Bool {get set}
}

class ContactList: contactListProtocol {
    
    private let contactWasFullyChanged = "ContactWasFullyChanged"
    private let contactWasChanged = "ContactWasChanged"
    private let contactWasDeleted = "ContactWasDeleted"
    
    private var contactList : [User] = []
    private let notifyCenter = NotificationCenter.default
    internal var showedContactUuid = String()
    internal var contactIsEdited = false
    private let networHelper = NetworkHelper()

    func addContact(contact: User) {
        contactList.append(contact)
    }
    
    func sortContactsBy(typeOfSort: String) -> [User] {
        
        if typeOfSort == "FirstName" {
            return contactList.sorted { ($0.name + $0.surname).localizedCaseInsensitiveCompare($1.name + $1.surname) == ComparisonResult.orderedAscending }
        } else {
            return contactList.sorted { ($0.surname + $0.name).localizedCaseInsensitiveCompare($1.surname + $1.name) == ComparisonResult.orderedAscending }
        }
    }

    func returnContactListCount() -> Int {
        
        return self.contactList.count
    }
    
    func editContactByUuid (contact: User){
            networHelper.updateUser(user: contact)
            var index = 0
            while index < contactList.count {
                if contactList[index].uiid == contact.uiid {
                    contactList[index] = contact
                }
                index += 1
        }
        
        notifyCenter.post(name: NSNotification.Name(rawValue: contactWasChanged), object: nil)
    }
    
    func getContactByIndex(index: Int) -> User {
        
        return contactList[index]
    }
    
    func getContactByUuid(uuid: String) -> User {
        let contact = contactList.filter{$0.uiid == uuid}
        let returnedContact: User = contact[0]
        return returnedContact
    }
    
    func removeElementBy(uuid: String, photoId: String?) {
        networHelper.deleteUser(uuid: uuid)
        if let id = photoId {
            if id != "" {
                self.networHelper.deletePhoto(id: id)
            }
        }
        contactList = contactList.filter{$0.uiid != uuid}
    }
    
    func saveContactList() {
       
        SaveDownloadContactList.save(array: self.contactList)
    }
    
    func loadContacts() {
            //string which is situated below is needed for work by usind NSCoder
            //self.contactList = SaveDownloadContactList.load() as! [User]
            networHelper.getAllUsers()
        }
    
    func getContactIsEdited() -> Bool {
        
        return self.contactIsEdited
    }
    
    func setContactIsEdited(state: Bool) {
        
        self.contactIsEdited = state
    }
    
    func setContactList(contacts: [User]) {
        self.contactList = contacts
    }
}
