//
//  MapViewController.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 6/16/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import UIKit
import MapKit

protocol MapViewControllerDelegate {
    func fillCoordinates(latitude: Double, longitude: Double)
}

class MapViewController: UIViewController,  MKMapViewDelegate{

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var adress: UILabel!
    
    var latitude: Double?
    var longitude: Double?
    
    let annotation = MKPointAnnotation()
    let geocoder = CLGeocoder()
    var delegate: MapViewControllerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        let location = CLLocation(latitude: latitude!, longitude: longitude!)
        centerMapOnLocation(location: location)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(triggerTouchAction))
        gestureRecognizer.delegate = self as? UIGestureRecognizerDelegate
        mapView.addGestureRecognizer(gestureRecognizer)
    }

    func centerMapOnLocation(location: CLLocation) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func triggerTouchAction(gestureReconizer: UILongPressGestureRecognizer) {
        
        let location = gestureReconizer.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        
        setUserLocation(coordinate)
    }
    
    func setUserLocation(_ coordinate: CLLocationCoordinate2D) {
        annotation.title = "You are here!"
        annotation.coordinate = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        mapView.addAnnotation(annotation)
        
        latitude = coordinate.latitude
        longitude = coordinate.longitude
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            var locationAdress = ""
            var countryCity = ""
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Address dictionary
            debugPrint(placeMark.addressDictionary as Any)
            
            // City
            if let city = placeMark.addressDictionary!["City"] as? String {
                debugPrint(city)
                
                countryCity = city
            }
            // Country
            if let country = placeMark.addressDictionary!["Country"] as? String {
                debugPrint(country)
                
                countryCity = countryCity.appending(" " + country)
            }
            
            if let adress = placeMark.addressDictionary!["Name"] as? String {
                
                locationAdress = adress
                debugPrint(adress)
            }
            
            self.city.text = countryCity
            self.adress.text = locationAdress
        })
    }

    @IBAction func saveLocation(_ sender: Any) {
        
        delegate?.fillCoordinates(latitude: latitude!, longitude: longitude!)
        self.navigationController?.popViewController(animated: true)
    }
}
