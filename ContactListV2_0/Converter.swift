//
//  Converter.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 6/26/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import Foundation
import UIKit

class Converter {
    
    func contactIntoJson(user: User) -> [String: Any] {
        var convertedValue = [String: Any]()
        
        convertedValue["user_id"] = user.uiid
        convertedValue["first_name"] = user.name
        convertedValue["last_name"] = user.surname
        convertedValue["email"] = user.eMail
        convertedValue["lat"] = user.latitude
        convertedValue["lon"] = user.longitude
        convertedValue["phone_number"] = user.phoneNumber
        
        return convertedValue
    }
    
    func convertFromJson(rawData : [String : Any]) -> User? {
        guard let uuid = rawData["userid"] as? String else { return nil }
        guard let firstName = rawData["firstname"] as? String else { return nil }
        guard let lastName = rawData["lastname"] as? String else { return nil }
        guard let eMail = rawData["email"] as? String else { return nil }
        let phoneNumber = rawData["phonenumber"] as? String
        let latitude = rawData["lat"] as? Double
        let longitude = rawData["lon"] as? Double
        let image = UIImage(named: "emptyContactPhoto")
        let imageToData: Data = UIImageJPEGRepresentation(image!, 1.0)!
        
        return User.init(firstName, lastName, phoneNumber!, eMail, uuid, imageToData, latitude!, longitude!)
    }
    
    func getImageIdFromJson(data: [String: Any]) -> String {
        let imageId = data["id"] as! String
        return imageId
    }
    
    func separateStrings(data: String) -> [String] {
        return data.components(separatedBy: ":")
    }
    
    func combineStrings(_ phone: String,_ photoId: String?) -> String {
        var combinedString = String()
        combinedString = phone
        combinedString.append(":")
        if let id = photoId {
            combinedString.append(id)
        } else {
            combinedString.append("")
        }
        return combinedString
    }
}
