//
//  NetworkHellper.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 6/22/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//
//tokens
// access_token=139131bdaa796206c2327275249a0a0a7da54b2e&expires_in=2419200&token_type=bearer&
// refresh_token=450b960286c368328e112ad8626cc093eaeb1a01&account_username=horBog&account_id=65313395

import Foundation
import UIKit

protocol NetworkProtocol {
    var baseUrl: String {get}
    func getAllUsers()
    func addUser()
    func updateUser(UUID: String)
    func deleteUser(UUID: String)
}

class NetworkHelper{
    
    let bundleIdentifier =  Bundle.main.bundleIdentifier
    let converter = Converter();
    final var baseUrl = "http://10.24.9.10:8080/user"
    final var aditionalUrlWithAppId = "?app_id="
    final var allUsers = "/all"

    let clientId = "1b8ab4afeac321a"
    let clientSecret = "e9240e4b7ea6d95efb30e2f43df6009fc3bca857"
    let accessToken = "139131bdaa796206c2327275249a0a0a7da54b2e"
        
    private let notifyCenter = NotificationCenter.default
    
    func getAllUsers() {
        var contacts = [User]()
        if let url = URL(string: baseUrl +  aditionalUrlWithAppId + bundleIdentifier!) {
            let session = URLSession.shared
            session.dataTask(with: url) { (data, response, error) in
                if let response = response {
                    debugPrint(response)
                }
                
                if let data = data {
                    do{
                        let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
                        
                        if let nodes = (jsonResult as? [String: Any])?["users"] as? [[String: Any]] {
                            
                            for contact in nodes {
                                let user = self.converter.convertFromJson(rawData: contact)
                                let phonePhotoId = self.converter.separateStrings(data: (user?.phoneNumber)!)
                                user?.phoneNumber = phonePhotoId[0]
                                user?.photoIdentifier = phonePhotoId[1]
                                contacts.append(user!)
                            }
                        }
                        self.notifyCenter.post(name: NSNotification.Name(rawValue: "ContactsWasDownloaded"), object: contacts)
                        debugPrint(jsonResult)
                    } catch {
                        debugPrint("error")
                    }
                    
                } else {
                    debugPrint("Contacts is epmty")
                }
                
                }.resume()
        }
    }
    
    func addUser(user: User) {
        
        let parameters = converter.contactIntoJson(user: user)
        guard let url = URL(string: baseUrl + aditionalUrlWithAppId + bundleIdentifier!) else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters) else {return}
        
        request.httpBody = httpBody
        
        let sesion = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if  let response = response {
                print(response)
            }
        }
        
        sesion.resume()
    }
    
    func updateUser(user: User) {
        
        let parameters = converter.contactIntoJson(user: user)
        
        let defaultSesion = URLSession(configuration: .default)
        var dataTask: URLSessionDataTask?
        dataTask?.cancel()
        guard let url = URL(string: "http://10.24.9.10:8080/user/" + user.uiid) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters) else {return}
        request.httpBody = httpBody
        
        do {
            
            var jsonResult = try JSONSerialization.jsonObject(with: httpBody, options: JSONSerialization.ReadingOptions.mutableContainers)
            debugPrint(jsonResult)
            
        } catch {
            
        }
        
        dataTask = defaultSesion.dataTask(with: request) { (data, response, error) in
            defer {dataTask = nil}
            if  let response = response as? HTTPURLResponse {
                debugPrint("Status: \(response.statusCode)")
            }
        }
        dataTask?.resume()
    }
    
    func deleteUser(uuid: String) {
        
        let defaultSesion = URLSession(configuration: .default)
        var dataTask: URLSessionDataTask?
        dataTask?.cancel()
        guard let url = URL(string: "http://10.24.9.10:8080/user/" + uuid) else {return}
        
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        
        dataTask = defaultSesion.dataTask(with: request) { (data, response, error) in
            defer {dataTask = nil}
            if  let response = response as? HTTPURLResponse {
                debugPrint("Status: \(response.statusCode)")
            }
        }
        dataTask?.resume()
    }
    
    func uploadPhoto(uuid: String, image: Data){
        
        let url = URL(string: "https://api.imgur.com/3/image")
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "POST"
        let image: Data = image //UIImagePNGRepresentation(UIImage(named: "emptyContactPhoto")!)!
        var body = Data()
        let defaultSesion = URLSession(configuration: .default)
        var dataTask: URLSessionDataTask?
        let fname = "\(uuid).png"
        var photoIdentifier = String()
        
        body.append(image)
        body.append("Content-Disposition:form-data; name=\"file\"; filename=\"\(fname)\"\r\n".data(using: .utf8)!)
        body.append("Content-Type: application/octet-stream\r\n\r\n".data(using: .utf8)!)
        body.append("\r\n".data(using: .utf8)!)
        
        request.httpBody = body
        request.setValue("Client-ID \(clientId)", forHTTPHeaderField: "Authorization")
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        
        dataTask = defaultSesion.dataTask(with: request) { (data, response, error) in
            defer {dataTask = nil}
            
            if let data = data{
                do{
                    let jsonResult = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
                    
                    if let data = (jsonResult as? [String: Any])?["data"] as? [String: Any] {
                        
                        let id = String(describing: self.converter.getImageIdFromJson(data: data))
                        debugPrint("IDENTIFIER IS \(String(describing: id))")
                       
                         self.notifyCenter.post(name: NSNotification.Name(rawValue: "ImageWasUpdated"), object: id)
                    }
                    debugPrint(jsonResult)
                } catch {
                    debugPrint("error")
                }
            }
            if  let response = response as? HTTPURLResponse {
                debugPrint("Response here: \(response)")
            }
        }
        dataTask?.resume()
    }
    
    func deletePhoto(id: String?) {
        
        let defaultSesion = URLSession(configuration: .default)
        var dataTask: URLSessionDataTask?
        dataTask?.cancel()
        var ids = ""
        if let identifier = id {
            debugPrint(identifier)
            ids = identifier
        }
        guard let url = URL(string: "https://api.imgur.com/3/image/" + ids) else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        dataTask = defaultSesion.dataTask(with: request) { (data, response, error) in
            defer {dataTask = nil}
            if  let response = response as? HTTPURLResponse {
                debugPrint("Status: \(response.statusCode)")
            }
        }
        dataTask?.resume()
    }
    
    func downloadPhoto(uuid: String) {
        
        
    }
}
