//
//  ViewController.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 5/17/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MapViewControllerDelegate {
    
    @IBOutlet weak var txtfFirstName: UITextField!
    @IBOutlet weak var txtfSecondName: UITextField!
    @IBOutlet weak var txtfPhoneNumber: UITextField!
    @IBOutlet weak var txtfEmail: UITextField!
    @IBOutlet weak var btnDeleteEditedContact: UIButton!
    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var btnLocation: UIButton!
    
    private let networkHelper = NetworkHelper()
    private let converter = Converter()
    private let notifyCenter = NotificationCenter.default
    private let editedContactWasDeleted = "EditedContactWasDeleted"
    private let allertGreeting = "Welcome to PhotoManager"
    private let allertMassage = "Could you choose what to do with photo?"
    private var locationInformation: String? = nil
    private let picker = UIImagePickerController()
    private let nsNotify = NotificationCenter.default
    private var defaultLatitude = 49.23
    private var defaultLongitude = 28.46
    private var contactList = (UIApplication.shared.delegate as! AppDelegate).contactList
    private var contact : User?
    private var processedContact: User?
    private var photoWasChanged = false
    private var defaultPhotoWasUsed: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setGeadient()
        mapView.isScrollEnabled = false
        picker.delegate = self
        deleteEditedOnOff()
        setView()

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(triggerTouchAction))
        gestureRecognizer.delegate = self as? UIGestureRecognizerDelegate
        userAvatar.isUserInteractionEnabled = true
        self.userAvatar.addGestureRecognizer(gestureRecognizer)
        notifyCenter.addObserver(forName: Notification.Name(rawValue:"ImageWasUpdated"), object: nil, queue: nil, using: catchNotification)
    }
    
    func catchNotification(notification:Notification) -> Void {
        if notification.name.rawValue == "ImageWasUpdated" {
            let phone = processedContact?.phoneNumber
            let photoId = notification.object as! String
            let phonePhotoId = converter.combineStrings(phone!, photoId)
            processedContact?.phoneNumber = phonePhotoId
            photoWasChanged == true ?  networkHelper.addUser(user: processedContact!) : networkHelper.updateUser(user: processedContact!)
            processedContact?.phoneNumber = phone!
            processedContact?.photoIdentifier = photoId
        }
        notifyCenter.post(name: NSNotification.Name(rawValue: "ContactWasChanged"), object: nil)
    }

    func setView() {
        
        if  contactList.getContactIsEdited() == true {
            contact = contactList.getContactByUuid(uuid: contactList.showedContactUuid)
            self.title = self.contact!.name + " " + self.contact!.surname
            self.txtfFirstName.text = contact?.name
            self.txtfSecondName.text = self.contact?.surname
            self.txtfPhoneNumber.text = self.contact?.phoneNumber
            self.txtfEmail.text = self.contact?.eMail
            let image = UIImage(data: (self.contact?.avatar)!,scale:1.0)
            self.userAvatar.image = image
            self.defaultLatitude = (self.contact?.latitude)!
            self.defaultLongitude = (self.contact?.longitude)!
            self.showLocationInformation(latitude: (contact?.latitude)!, longitude: (contact?.longitude)!)
        } else {
            
            userAvatar.image = #imageLiteral(resourceName: "emptyContactPhoto")
        }
    }
    
    func triggerTouchAction(gestureReconizer: UILongPressGestureRecognizer) {
        
        let alertController = UIAlertController(title: allertGreeting, message: allertMassage, preferredStyle: .actionSheet)
        
        let choosePhotoButton = UIAlertAction(title: "Get from media", style: .default, handler: { (action) -> Void in
            self.editPhoto()
            self.photoWasChanged = true
            self.defaultPhotoWasUsed = false
        })
        
        let  clearButton = UIAlertAction(title: "Clear", style: .destructive, handler: { (action) -> Void in
            self.userAvatar.image = #imageLiteral(resourceName: "emptyContactPhoto")
            self.photoWasChanged = false
            self.defaultPhotoWasUsed = true
        })
        
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) -> Void in
        })
        alertController.addAction(choosePhotoButton)
        alertController.addAction(clearButton)
        alertController.addAction(cancelButton)
        self.navigationController!.present(alertController, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userAvatar.contentMode = .scaleAspectFit
            userAvatar.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func btnSave(_ sender: Any) {
       
        guard let firstName = txtfFirstName.text else {
            txtfFirstName.becomeFirstResponder()
            return
        }
        guard let lastName = txtfSecondName.text else {
            txtfSecondName.becomeFirstResponder()
            return
        }
        guard let phoneNumber = txtfPhoneNumber.text else {
            txtfPhoneNumber.becomeFirstResponder()
            return
        }
        guard let eMail = txtfEmail.text else {
            txtfEmail.becomeFirstResponder()
            return
        }
        
        let userAvatarInData : Data = UIImageJPEGRepresentation(userAvatar.image!, 1.0)!
        
        if contactList.getContactIsEdited() == false {
            
            let createUuid = UUID().uuidString
            let newContact = User(firstName,
                                  lastName,
                                  phoneNumber,
                                  eMail,
                                  createUuid,
                                  userAvatarInData,
                                  defaultLatitude,
                                  defaultLongitude)
            
            if photoWasChanged == true {
                
                contactPhotoWasChanged(createUuid, userAvatarInData, nil)
                processedContact = newContact
                
            } else {
                newContact.phoneNumber.append(":")
                networkHelper.addUser(user: newContact)
                newContact.phoneNumber = phoneNumber
            }

            contactList.addContact(contact: newContact)

            //contactList.saveContactList()  For a home testing without server
        
            self.navigationController?.popToRootViewController(animated: true)
        } else {
            
            contact?.name = firstName
            contact?.surname = lastName
            contact?.eMail = eMail
            contact?.phoneNumber = phoneNumber
            contact?.avatar = userAvatarInData
            contact?.latitude = defaultLatitude
            contact?.longitude = defaultLongitude
            //contactList.saveContactList() For a home testing without server
            
            if photoWasChanged == true {
                contactPhotoWasChanged((contact?.uiid)!, (contact?.avatar)!, contact?.photoIdentifier)
            }
            
            if defaultPhotoWasUsed == true {
                if let photoId = contact?.photoIdentifier {
                    networkHelper.deletePhoto(id: (contact?.photoIdentifier)!)
                    contact?.photoIdentifier = ""
                }
            }
            
            contactList.editContactByUuid(contact: contact!)
            contactList.setContactIsEdited(state: false)
            
            //Add notification for refresh data in the "ShowInformationController"
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func contactPhotoWasChanged(_ uuid: String,_ image: Data,_ oldPhoneId: String?){
        networkHelper.uploadPhoto(uuid: uuid, image: image)
        if let oldPhoneIdentifier = oldPhoneId {
            networkHelper.deletePhoto(id: oldPhoneIdentifier)
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        
        if contactList.getContactIsEdited() == true {
            
            contactList.setContactIsEdited(state: false)
            self.navigationController?.popViewController(animated: true)
        } else {
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @IBAction func deleteEditedElement(_ sender: Any) {
        
        contactList.removeElementBy(uuid: contactList.showedContactUuid, photoId: contact?.photoIdentifier)
        notifyCenter.post(name: Notification.Name(rawValue: editedContactWasDeleted), object: nil)
        contactList.contactIsEdited = false
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
    func editPhoto() {
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        present(picker, animated: true, completion: nil)
        
    }
    
    //(REMINDER!!!) create mapKitHelper for this method
    func showLocationInformation(latitude: Double, longitude: Double) {
        
        self.locationInformation = ""
        let geoCoder = CLGeocoder()
        let annotation = MKPointAnnotation()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        let regionRadius: CLLocationDistance = 500
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius * 2.0,
                                                                  regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
        
        annotation.title = "You are here!"
        annotation.coordinate = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
        mapView.addAnnotation(annotation)

        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
        
            var fullAdress = ""
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            if let city = placeMark.addressDictionary!["City"] as? String {
                debugPrint(city)
    
                fullAdress = city
            }

            if let country = placeMark.addressDictionary!["Country"] as? String {
                debugPrint(country)

                fullAdress = fullAdress.appending(" " + country)
            }
            
            if let adress = placeMark.addressDictionary!["Name"] as? String {
                
                fullAdress = fullAdress.appending(" " + adress)
                debugPrint(adress)
            }
            self.btnLocation.setTitle(fullAdress , for: .normal)
        })
    }
    
    func fillCoordinates(latitude: Double, longitude: Double){
        showLocationInformation(latitude: latitude, longitude: longitude)
        defaultLatitude = latitude
        defaultLongitude = longitude
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap" {
            let destinationVC = segue.destination as! MapViewController
            destinationVC.delegate = self
            destinationVC.latitude = defaultLatitude
            destinationVC.longitude = defaultLongitude
        }
    }
    //(REMINDER!!!) watch if it possible to use one method for two views by passing "self.view"
    func setGeadient(){
        let firstColor = UIColor(red: 121/255, green: 204/255, blue: 251/255, alpha: 1.0).cgColor
        let secondColor = UIColor(red: 92/255, green: 239/255, blue: 252/255, alpha: 1.0).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [firstColor, secondColor]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 1.0)
        self.view.layer.insertSublayer(gradientLayer, at: 0)
    }

    func deleteEditedOnOff() {
        btnDeleteEditedContact.isHidden = contactList.getContactIsEdited() == true ? false : true
    }
}

