//
//  User.swift
//  ContactListV2_0
//
//  Created by Bohdan Hordiienko on 5/17/17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import Foundation

protocol UserInfo: NSCoding{
    var name: String {get set}
    var surname: String {get set}
    var phoneNumber: String {get set}
    var eMail: String {get set}
    var uiid : String {get set}
    var avatar: Data {get set}
    var latitude: Double {get set}
    var longitude: Double {get set}
    var photoIdentifier: String{get set}
    func encode(with aCoder: NSCoder)
    init?(coder aDecoder: NSCoder)
}

class User : NSObject, UserInfo {

    var name: String
    var surname: String
    var phoneNumber: String
    var eMail: String
    var uiid: String
    var avatar: Data
    var latitude: Double
    var longitude: Double
    var photoIdentifier: String = ""
    
    init(_ name: String,_ surname: String,_ phoneNumber: String,_ eMail:String,_ uuid: String,_ avatar: Data,_ latitude: Double,_ longitude: Double) {
        
        self.name = name
        self.surname = surname
        self.phoneNumber = phoneNumber
        self.eMail = eMail
        self.uiid = uuid
        self.avatar = avatar
        self.latitude = latitude
        self.longitude = longitude
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(self.name,forKey: "name")
        aCoder.encode(self.surname,forKey: "surname")
        aCoder.encode(self.phoneNumber,forKey: "phoneNumber")
        aCoder.encode(self.eMail,forKey: "eMail")
        aCoder.encode(self.uiid,forKey: "uiid")
        aCoder.encode(self.avatar,forKey: "avatar")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: "name") as? String,
            let surname = aDecoder.decodeObject(forKey: "surname") as? String,
            let phoneNumber = aDecoder.decodeObject(forKey: "phoneNumber") as? String,
            let eMail = aDecoder.decodeObject(forKey: "eMail") as? String,
            let uiid = aDecoder.decodeObject(forKey: "uiid") as? String,
            let avatar = aDecoder.decodeObject(forKey: "avatar") as? Data
            else {return nil}
        
        let lan = 49.23
        let lon = 28.21
        self.init(name, surname, phoneNumber, eMail, uiid, avatar, lan, lon)
    }
}


