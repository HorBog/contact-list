//
//  TableViewController.swift
//  ContactListV2_0
//
//  Created by Богдан on 18.05.17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import UIKit
import MBProgressHUD

class TableViewController: UITableViewController {
    
    private var contactList = (UIApplication.shared.delegate as! AppDelegate).contactList
    private var sortedContacsList: [User] = []
    private let nc = NotificationCenter.default
    
    @IBOutlet weak var btnEdit: UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 150
        tableView.tableFooterView = UIView()
        downloadingProgress()
        contactList.loadContacts()
        sortedContacsList = contactList.sortContactsBy(typeOfSort: "FirstName")
        btnEditOnOff()
        nc.addObserver(forName: Notification.Name(rawValue:"ContactWasFullyChanged"), object: nil, queue: nil, using: catchNotification)
        nc.addObserver(forName: Notification.Name(rawValue:"EditedContactWasDeleted"), object: nil, queue: nil, using: catchNotification)
        nc.addObserver(forName: Notification.Name(rawValue:"ContactWasChanged"), object: nil, queue: nil, using: catchNotification)
        nc.addObserver(forName: Notification.Name(rawValue:"ContactsWasDownloaded"), object: nil, queue: nil, using: catchNotification)
   
        self.navigationItem.leftBarButtonItem = self.editButtonItem
    }
    
    func catchNotification(notification:Notification) -> Void {
        debugPrint("\(notification)")
        if notification.name.rawValue == "ContactWasChanged" {
            sortedContacsList = contactList.sortContactsBy(typeOfSort: "FirstName")
            tableView.reloadData()
        } else if notification.name.rawValue == "ContactWasFullyChanged" {
            sortedContacsList = contactList.sortContactsBy(typeOfSort: "FirstName")
            tableView.reloadData()
        } else if notification.name.rawValue == "EditedContactWasDeleted" {
            sortedContacsList = contactList.sortContactsBy(typeOfSort: "FirstName")
            btnEditOnOff()
            tableView.reloadData()
        } else if notification.name.rawValue == "ContactsWasDownloaded" {
            contactList.setContactList(contacts: notification.object as! [User])
            sortedContacsList = contactList.sortContactsBy(typeOfSort: "FirstName")
            btnEditOnOff()
            tableView.reloadData()
            
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
            }
        }
    }
    
    func downloadingProgress() {
        
        let progressHug =  MBProgressHUD.showAdded(to: self.view, animated: true)
     
            progressHug.label.text = "Loading..."
            progressHug.detailsLabel.text = "Please Wait!!!"
            progressHug.graceTime = 0.5
            progressHug.isUserInteractionEnabled = true
    }
    
    func btnEditOnOff() {
        
        if contactList.returnContactListCount() < 1 {
            
            self.editButtonItem.isEnabled = false
            self.editButtonItem.tintColor = UIColor.clear
           
        } else {

            self.editButtonItem.isEnabled = true
            self.editButtonItem.tintColor = nil
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactList.returnContactListCount()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactViewCell
        let cellContactList = contactList.getContactByUuid(uuid: sortedContacsList[indexPath.row].uiid)
        
        cell.firstName.text = cellContactList.name
        cell.lastName.text = cellContactList.surname
        cell.phoneNumber.text = cellContactList.phoneNumber
        cell.userAvatar.image = UIImage(data: cellContactList.avatar,scale:1.0)
        cell.fullNameTxt = cellContactList.name + " " + cellContactList.surname
        cell.firstNameTxt = cellContactList.name
        cell.eMail.text = cellContactList.eMail
        cell.buttonState = false
        cell.changeCellExpand()
        
        cell.accessoryType = .disclosureIndicator

        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let degree : Double = 90
        let rotationAngle = CGFloat(degree * .pi / 180)
        let rotationTransform = CATransform3DMakeRotation(rotationAngle, 1, 0, 0)
        cell.layer.transform = rotationTransform
        
        UIView.animate(withDuration: 1, delay: 0.1 * Double(indexPath.row), options: .curveEaseOut, animations: {
            cell.layer.transform = CATransform3DIdentity
        })
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if contactList.returnContactListCount() > indexPath.row {

            let neededUiid = sortedContacsList[indexPath.row].uiid
            contactList.showedContactUuid = neededUiid
        }
        return indexPath
    }

        override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let neededUiid = sortedContacsList[indexPath.row].uiid
            contactList.removeElementBy(uuid: neededUiid, photoId: sortedContacsList[indexPath.row].photoIdentifier)
            sortedContacsList = contactList.sortContactsBy(typeOfSort: "FirstName")
            contactList.saveContactList()
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
}
