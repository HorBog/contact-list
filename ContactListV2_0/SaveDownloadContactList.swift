//
//  SaveDownloadContactList.swift
//  ContactListV2_0
//
//  Created by Богдан on 18.05.17.
//  Copyright © 2017 Bohdan Hordiienko. All rights reserved.
//

import Foundation

class SaveDownloadContactList {
    
    static func save(array: [UserInfo]) {
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = DocumentDirURL.appendingPathComponent("contacts").path
        NSKeyedArchiver.archiveRootObject(array, toFile: fileURL)
    }
    
    static func load()->[UserInfo] {
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = DocumentDirURL.appendingPathComponent("contacts").path
        var array = [UserInfo]()
        
        if let ourDate = NSKeyedUnarchiver.unarchiveObject(withFile: fileURL) as? [UserInfo]{
            array = ourDate
        }
        return array
    }
}

